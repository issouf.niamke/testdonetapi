﻿using Microsoft.AspNetCore.Mvc;
using FilmApi.Models;
using FilmApi.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;

namespace FilmApi.Controllers
{
   

    [Authorize(Policy = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class FilmsController : ControllerBase
    {

 private readonly ILogger<FilmsController> _logger;
  private readonly FilmContext _context;
        private readonly IConfiguration _configuration;

        public FilmsController(FilmContext context, IConfiguration configuration,ILogger<FilmsController> logger)
        {
            _context = context;
            _configuration = configuration;
            _logger = logger;
        }

        // GET: api/Films
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Film>>> GetFilms()
        {
            return await _context.Films.ToListAsync();
        }

        // GET: api/Films/5
        [Authorize(Roles = "Admin")]
        [HttpGet("{id}")]
        public async Task<ActionResult<Film>> GetFilm(int id)
        {
            var film = await _context.Films.FindAsync(id);

            if (film == null)
            {
                return NotFound();
            }

            return film;
        }

        // POST: api/Films
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<Film>> PostFilm(Film film)
        {
            _context.Films.Add(film);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetFilm), new { id = film.Id }, film);
        }

        // PUT: api/Films/5
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFilm(int id, Film film)
        {
            if (id != film.Id)
            {
                 _logger.LogError("Operation failed");
                return BadRequest();
            }

            _context.Entry(film).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                _logger.LogInformation("Operation succeeded");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FilmExists(id))
                {
                     _logger.LogError("Operation failed");
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // DELETE: api/Films/5
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFilm(int id)
        {
            var film = await _context.Films.FindAsync(id);
            if (film == null)
            {
                return NotFound();
            }

            _context.Films.Remove(film);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FilmExists(int id)
        {
            return _context.Films.Any(e => e.Id == id);
        }
 // POST: api/Films/Login
        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult Login([FromBody] UserLogin model)
        {
            // Validate the user credentials (example)
            var isValid = IsValidUser(model);

            if (isValid)
            {
                var tokenString = GenerateJwtToken(model.Username);
                return Ok(new { Token = tokenString });
            }

            return Unauthorized();
        }

        // Method to validate user credentials (example)
        private bool IsValidUser(UserLogin model)
        {
            // Implement your logic to validate the user (e.g., check against database)
            // For demo purposes, just return true if username and password match
            return model.Username == "admin" && model.Password == "password";
        }

        // Method to generate JWT token
        private string GenerateJwtToken(string username)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
              _configuration["Jwt:Issuer"],
              new[] { new Claim(ClaimTypes.Name, username) },
              expires: DateTime.Now.AddHours(1),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
    }

