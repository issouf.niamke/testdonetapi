﻿using Microsoft.EntityFrameworkCore;
using FilmApi.Models;

namespace FilmApi.Data
{
    public class FilmContext : DbContext
    {
        public FilmContext(DbContextOptions<FilmContext> options) : base(options) { }

        public DbSet<Film> Films { get; set; }
    }
}
