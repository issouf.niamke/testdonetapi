﻿namespace FilmApi.Models
{
    public class Film
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Titre { get; set; }
        public string Description { get; set; }
        public int Duree { get; set; }
        public string Auteur { get; set; }
    }
}